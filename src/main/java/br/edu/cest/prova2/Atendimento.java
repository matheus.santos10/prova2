package br.edu.cest.prova2;

import java.util.Date;

public class Atendimento {
	private Date data;
    private int numero;
    
    
    public Date getData() {
   	 return data;
    }
    public void setData(Date data) {
   	 this.data = data;
    }
    public int getNumero() {
   	 return numero;
    }
    public void setNumero(int numero) {
   	 this.numero = numero;
    }
    

}
