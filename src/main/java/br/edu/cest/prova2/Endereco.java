package br.edu.cest.prova2;

public class Endereco {
	private String logradouro;
    private Cidade cidade;

    public Cidade getCidade() {
    	return cidade;
    }
    

    public void setCidade(Cidade cidade) {
   	 	this.cidade = cidade;
    }




    public String getLogradouro() {
   	 	return logradouro;
    }

    public void setLogradouro(String logradouro) {
   	 	this.logradouro = logradouro;
    }

}
